<?php
/*
Plugin Name:  WP Algolia
Plugin URI:   https://packagist.org/packages/schaapdigitalcreatives/wp-algolia
Description:  The WordPress plugin to enable Algolia indexing.
Version:      1.4.11
Author:       Schaap Digital Creatives
Author URI:   https://schaapontwerpers.nl/
License:      GNU General Public License
*/

new WordPressAlgolia\Init();
