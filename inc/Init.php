<?php

namespace WordPressAlgolia;

use WordPressPluginAPI\Manager;
use WP_CLI;

class Init
{
    private $hooks;

    public function __construct()
    {
        WP_CLI::add_command(
            'algolia',
            'WordPressAlgolia\CLI\Algolia',
        );

        $this->hooks = [
            new Admin\OptionsPages(),
            new Admin\Sync(),
        ];

        // Run PluginAPI registration
        $manager = new Manager();
        foreach ($this->hooks as $class) {
            $manager->register($class);
        }
    }
}
