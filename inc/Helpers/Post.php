<?php

namespace WordPressAlgolia\Helpers;

use Exception;
use Smalot\PdfParser\Parser;
use Groups_Post_Access;
use WP_Post;
use WP_Term;

class Post
{
    const MAX_RECORD_SIZE = 10000;

    private $post;

    public function __construct(WP_Post $post)
    {
        $this->post = $post;
    }

    public function getRecords(): array
    {
        $records = [];
        $frontendUrl = apply_filters(
            'sdc_algolia_frontend_url',
            get_field('frontend_url', 'options') ?: ''
        );

        $author = get_user_by(
            'ID',
            $this->post->post_author
        );

        $content = $this->getCleanContent($this->post->post_content);

        $record = apply_filters(
            'sdc_algolia_record',
            [
                'objectID' => implode(
                    '#',
                    [$this->post->post_type, $this->post->ID]
                ),
                'type' => [
                    'slug' => $this->post->post_type,
                    'name' => get_post_type_object(
                        $this->post->post_type
                    )->label,
                ],
                'order' => $this->post->menu_order,
                'slug' => $this->post->post_name,
                'title' => $this->post->post_title,
                'link' => str_replace(
                    home_url(),
                    $frontendUrl,
                    get_permalink($this->post->ID),
                ),
                'date' => strtotime($this->post->post_date),
                'year' => get_the_date('Y', $this->post),
                'author' => [
                    'id' => $this->post->post_author,
                    'name' => $author ? $author->display_name : "",
                ],
                'image' => $this->getImage(),
                'excerpt' => $this->post->post_excerpt,
                'content' => $content,
                'terms' => $this->getTerms(),
                'acf' => get_fields($this->post->ID) ?: [],
            ],
            $this->post
        );

        if ($this->post->post_type === 'event') {
            $startDate = get_field('start_date', $this->post->ID) ?: $this->post->post_date;
            $endDate = get_field('end_date', $this->post->ID) ?: $this->post->post_date;

            $record['startDate'] = strtotime($startDate);
            $record['endDate'] = strtotime($endDate);
            $record['endYear'] = date('Y', strtotime($endDate));
        }

        $this->addVisibleBy($record);

        $recordSize = strlen(json_encode($record));
        $iteration = 1;

        if ($recordSize < self::MAX_RECORD_SIZE) {
            array_push($records, $record);
        } else {
            $splitRecords = $this->splitRecord(
                $record,
                $content,
                $record['objectID'],
                $iteration
            );

            foreach ($splitRecords as $splitRecord) {
                array_push($records, $splitRecord);
            }
        }

        $attachments = apply_filters(
            'sdc_algolia_record_attachments',
            [],
            $this->post
        );

        if (count($attachments) > 0) {
            $attachmentsContent = $this->getAttachmentsContent($attachments);

            $splitRecords = $this->splitRecord(
                $record,
                $attachmentsContent,
                $record['objectID'],
                $iteration
            );

            foreach ($splitRecords as $splitRecord) {
                array_push($records, $splitRecord);
            }
        }

        return $records;
    }

    private function splitRecord(
        array $record,
        string $content,
        string $objectID,
        int &$iteration
    ): array {
        $record['content'] = '';
        $recordSizeWithoutContent = strlen(json_encode($record)) + 300;
        $maxContentSize = self::MAX_RECORD_SIZE - $recordSizeWithoutContent;

        $contentSplits = explode("\n", wordwrap($content, $maxContentSize));

        $records = [];

        foreach ($contentSplits as $contentSplit) {
            $record['objectID'] = $objectID . '#' . $iteration;
            $record['content'] = $contentSplit;
            array_push($records, $record);

            $iteration++;
        }

        return $records;
    }

    private function getAttachmentsContent(array $attachments): string
    {
        $content = '';

        foreach ($attachments as $attachment) {
            if (
                is_array($attachment) &&
                array_key_exists('type', $attachment) &&
                array_key_exists('file', $attachment) &&
                $attachment['type'] === 'pdf'
            ) {
                try {
                    $parser = new Parser();
                    $pdf = $parser->parseFile($attachment['file']);

                    $pdfText = $pdf->getText();

                    $content .= mb_detect_encoding($pdfText) ?
                        mb_convert_encoding(
                            $pdfText,
                            "UTF-8",
                            mb_detect_encoding($pdfText)
                        ) :
                        '';
                } catch (Exception $e) {
                    error_log($e);
                }
            }
        }

        return $this->getCleanContent($content);
    }

    private function getCleanContent(string $uncleanContent): string
    {
        $content = $uncleanContent;

        // Make sure there's actual HTML tags
        $content = str_replace('&lt;', '<', $content);

        // Replace <br> with a \n
        $content = str_replace("<br>", "\n", $content);

        // Strip tags
        $content = wp_strip_all_tags($content);

        // Replace one or more new lines with a single space
        $content = preg_replace('/\s+/', ' ', $content);

        // Remove spaces from start and end of string
        $content = trim($content);

        return $content;
    }

    private function getImage(): array
    {
        // Only proceed if the post has a featured image.
        $imageId = get_post_thumbnail_id($this->post->ID);
        $imageData = [];

        if ($imageId) {
            $alt = get_post_meta($imageId, '_wp_attachment_image_alt', true);
            $metadata = wp_get_attachment_metadata($imageId);
            $url = wp_get_attachment_url($imageId);

            $imageData = [
                'alt' => $alt,
                'url' => $url,
            ];

            if (is_array($metadata)) {
                if (array_key_exists('width', $metadata)) {
                    $imageData['width'] = $metadata['width'];
                }

                if (array_key_exists('height', $metadata)) {
                    $imageData['height'] = $metadata['height'];
                }
            }
        }

        return $imageData;
    }

    /**
     * Get the post terms from all taxonomies or a single post
     */
    private function getTerms(): array
    {
        $taxonomies = [];
        $postTaxonomies = get_post_taxonomies($this->post->ID);

        foreach ($postTaxonomies as $postTaxonomy) {
            $taxonomy = get_taxonomy($postTaxonomy);

            $terms = wp_get_post_terms(
                $this->post->ID,
                $postTaxonomy,
                [
                    'fields' => 'id=>name',
                ],
            );

            $hierarchicalTerms = $this->sortTermsHierarchically(
                $terms,
                $postTaxonomy
            );

            $taxonomies[$postTaxonomy] = [
                'label' => $taxonomy->label,
                'names' => $hierarchicalTerms,
            ];
        }

        return $taxonomies;
    }

    /**
     * Sort terms hierarchically
     */
    private function sortTermsHierarchically(
        array $terms,
        string $taxonomy
    ): array {
        $names = [];

        foreach ($terms as $termId => $selectedTermName) {
            $term = get_term_by('id', $termId, $taxonomy);

            if ($term->parent === 0) {
                array_push($names, $selectedTermName);
            } else {
                $name = $this->addParentName(
                    $term,
                    $selectedTermName,
                    $taxonomy
                );

                $termNames = explode(' > ', $name);
                $termNamesCount = count($termNames);
                $i = 0;

                for ($i; $i < $termNamesCount; $i += 1) {
                    $indexName = implode(
                        ' > ',
                        array_slice($termNames, 0, $i + 1)
                    );

                    if (!in_array($indexName, $names)) {
                        array_push($names, $indexName);
                    }
                }
            }
        }

        return $names;
    }

    private function addParentName(
        WP_Term $childTerm,
        string $name,
        string $taxonomy
    ): string {
        $termName = $name;
        $parentTerm = get_term_by('id', $childTerm->parent, $taxonomy);

        if ($parentTerm) {
            $termName = $parentTerm->name . ' > ' . $name;

            if ($parentTerm->parent !== 0) {
                $termName = $this->addParentName(
                    $parentTerm,
                    $termName,
                    $taxonomy
                );
            }
        }

        return $termName;
    }

    private function addVisibleBy(&$record)
    {
        $groupsWithAccess = [];

        if (
            class_exists('Groups_Post_Access') &&
            class_exists('Groups_Group')
        ) {
            $groupsWithAccess = $this->getGroupWithAccessToPost();

            // Standard visible by Everybody
            if (count($groupsWithAccess) === 0) {
                $groupsWithAccess[] = "Everybody";
            }
        } else {
            $groupsWithAccess[] = "Everybody";
        }

        $record['visible_by'] = $groupsWithAccess;
    }

    /**
     * Get the access level groups for the Post
     */
    private function getGroupWithAccessToPost()
    {
        $groupsWithAccess = [];

        if (
            class_exists('Groups_Post_Access') &&
            class_exists('Groups_Group')
        ) {
            $groupsPostAccess = new Groups_Post_Access();

            // Get the group IDs that have read access to the post
            $groupsWithAccess = $groupsPostAccess->get_read_group_ids(
                $this->post->ID
            );
        }

        return $groupsWithAccess;
    }
}
