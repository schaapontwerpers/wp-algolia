<?php

namespace WordPressAlgolia\Index;

use WP_CLI;

class Sorting
{
    private $client;

    public function __construct()
    {
        // Force locale
        global $locale;
        switch_to_locale($locale);

        $this->client = new Client();
        $this->client->setIndex('posts');
    }

    public function setReplicas(array $args = [], array $assocArgs = [])
    {
        $replicas = [
            $this->client->indexName . '_date_desc',
            $this->client->indexName . '_date_asc',
            $this->client->indexName . '_startDate_desc',
            $this->client->indexName . '_startDate_asc',
            $this->client->indexName . '_title_desc',
            $this->client->indexName . '_title_asc',
            $this->client->indexName . '_order_asc',
        ];

        $this->client->index->setSettings([
            'replicas' => $replicas
        ]);

        foreach ($replicas as $replica) {
            $this->client->setIndex($replica);

            $replicaParts = explode('_', $replica);
            $orderIndex = count($replicaParts) - 1;
            $orderbyIndex = count($replicaParts) - 2;
            $ranking = $replicaParts[$orderIndex] .
                '(' .
                $replicaParts[$orderbyIndex] .
                ')';

            $this->client->index->setSettings([
                'ranking' => ($replicaParts[$orderbyIndex] === 'order') ?
                    [$ranking, 'desc(date)', 'asc(title)'] :
                    [$ranking],
                'attributeForDistinct' => 'link',
                'distinct' => true,
                'searchableAttributes' => apply_filters(
                    'sdc_algolia_searchable_attributes',
                    [
                        'acf',
                        'author',
                        'content',
                        'excerpt',
                        'id',
                        'slug',
                        'terms',
                        'title',
                        'type',
                    ]
                ),
            ]);

            $taxonomies = new Taxonomies($replica, false);
            $taxonomies->setFacets();
        }

        WP_CLI::success("Sorting replicas created.");
    }
}
