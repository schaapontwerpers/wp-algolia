<?php

namespace WordPressAlgolia\Index;

use WordPressAlgolia\Helpers\Post;
use WP_Query;
use WP_CLI;

class Posts
{
    private $client;

    public function __construct()
    {
        // Force locale
        global $locale;
        switch_to_locale($locale);

        $this->client = new Client();
        $this->client->setIndex('posts');
    }

    public function reindex(array $args = [], array $assocArgs = [])
    {
        $this->client->index->clearObjects()->wait();

        $publicPostTypes = get_post_types(['public' => true]);
        $paged = 1;
        $count = 0;

        do {
            $current_user = wp_get_current_user();

            wp_set_current_user(1);

            $posts = new WP_Query(apply_filters(
                'sdc_algolia_index_query',
                [
                    'posts_per_page' => 100,
                    'paged' => $paged,
                    'post_type' => $publicPostTypes,
                ]
            ));

            wp_set_current_user($current_user->ID);

            if (!$posts->have_posts()) {
                break;
            }

            $records = [];

            foreach ($posts->posts as $post) {
                if (!empty($assocArgs['verbose'])) {
                    WP_CLI::line('Serializing [' . $post->post_title . ']');
                }

                $postHelper = new Post($post);
                $postRecords = $postHelper->getRecords();

                foreach ($postRecords as $record) {
                    array_push($records, $record);
                }

                $count++;
            }

            $maxPages = $posts->max_num_pages;
            WP_CLI::line("Sending records from page $paged/$maxPages...");

            $this->client->index->saveObjects($records);

            $paged++;
        } while (true);

        $this->client->index->setSettings([
            'attributeForDistinct' => 'link',
            'distinct' => true,
            'searchableAttributes' => apply_filters(
                'sdc_algolia_searchable_attributes',
                [
                    'acf',
                    'author.name',
                    'content',
                    'excerpt',
                    'slug',
                    'terms',
                    'title',
                    'type.name',
                ]
            ),
        ]);

        WP_CLI::success("$count posts indexed.");
    }
}
