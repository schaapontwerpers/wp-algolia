<?php

namespace WordPressAlgolia\Index;

use Algolia\AlgoliaSearch\SearchClient;

class Client
{
    private $client;

    public $clientExists = false;

    public $index;

    public $indexName;

    public function __construct()
    {
        $applicationId = get_field('algolia_application_id', 'options');
        $apiKey = get_field('algolia_api_key', 'options');

        $this->clientExists = $applicationId &&
            $applicationId !== '' &&
            $apiKey &&
            $apiKey !== '';
        $this->client = $this->clientExists ?
            SearchClient::create($applicationId, $apiKey) :
            null;
    }

    public function setIndex($index)
    {
        $this->indexName = $index;
        $this->index = $this->client->initIndex($this->indexName);
    }
}
