<?php

namespace WordPressAlgolia\Index;

use WP_CLI;

class Taxonomies
{
    private $client;

    public function __construct($indexName = 'posts')
    {
        // Force locale
        global $locale;
        switch_to_locale($locale);

        $this->client = new Client();
        $this->client->setIndex($indexName);
    }

    public function setFacets(array $args = [], array $assocArgs = [])
    {
        $facets = [
            'afterDistinct(author.name)',
            'afterDistinct(type.name)',
            'afterDistinct(year)',
        ];

        $taxonomies = get_taxonomies();

        foreach ($taxonomies as $taxonomy) {
            $facets[] = 'afterDistinct(terms.' . $taxonomy . '.names)';
        }

        // Add visible_by as filterOnly attribute
        $facets[] = 'filterOnly(visible_by)';

        $this->client->index->setSettings([
            'attributesForFaceting' => $facets,
            'unretrievableAttributes' => ['visible_by']
        ]);


        WP_CLI::success("Taxonomies created as facets.");
    }
}
