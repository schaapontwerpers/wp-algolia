<?php

namespace WordPressAlgolia\Admin;

use WordPressAlgolia\Index\Client;
use WordPressAlgolia\Helpers\Post;
use WordPressPluginAPI\ActionHook;
use WP_Post;

class Sync implements ActionHook
{
    /**
     * Subscribe functions to corresponding actions
     */
    public static function getActions(): array
    {
        return [
            'wp_after_insert_post' => ['addPost', 10, 4],
            'wp_trash_post' => ['removePost', 10, 2],
        ];
    }

    public function addPost(
        int $id,
        WP_Post $post,
        bool $update,
        WP_Post|null $postBefore
    ): WP_Post {
        if (
            wp_is_post_revision($id) ||
            wp_is_post_autosave($id)
        ) {
            return $post;
        }

        $publicPostTypes = get_post_types(['public' => true]);

        if (in_array($post->post_type, $publicPostTypes)) {
            $shouldIndex = apply_filters(
                'sdc_algolia_should_index',
                true,
                $post
            );

            if ($shouldIndex) {
                $client = new Client();

                if ($client->clientExists) {
                    $client->setIndex('posts');

                    if ($post->post_status === 'publish') {
                        $postHelper = new Post($post);
                        $records = $postHelper->getRecords();

                        foreach ($records as $record) {
                            $client->index->saveObject($record);
                        }
                    } else if ($postBefore && $postBefore->post_status === 'publish') {
                        $postHelper = new Post($postBefore);
                        $records = $postHelper->getRecords();

                        foreach ($records as $record) {
                            $client->index->deleteObject($record['objectID']);
                        }
                    }
                }
            }
        }

        return $post;
    }

    public function removePost(int $id, string $previousState)
    {
        $post = get_post($id);
        $publicPostTypes = get_post_types(['public' => true]);

        if (
            in_array($post->post_type, $publicPostTypes) &&
            $previousState === 'publish'
        ) {
            $postHelper = new Post($post);
            $records = $postHelper->getRecords();

            $client = new Client();

            if ($client->clientExists) {
                $client->setIndex('posts');

                foreach ($records as $record) {
                    $client->index->deleteObject($record['objectID']);
                }
            }
        }

        return $post;
    }
}
