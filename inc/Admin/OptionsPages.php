<?php

namespace WordPressAlgolia\Admin;

use WordPressPluginAPI\ActionHook;
use WordPressPluginAPI\FilterHook;

class OptionsPages implements ActionHook, FilterHook
{
    /**
     * Subscribe functions to corresponding actions
     */
    public static function getActions(): array
    {
        return [
            'acf/init' => 'addPages',
        ];
    }

    /**
     * Subscribe functions to corresponding actions
     */
    public static function getFilters(): array
    {
        return [
            'acf/settings/load_json' => 'addPluginJson',
        ];
    }

    public function addPluginJson(array $paths): array
    {
        // Append the new path and return it.
        $paths[] = WPMU_PLUGIN_DIR . '/sdc-wp-algolia/acf-json';

        return $paths;
    }

    /**
     * Add options pages
     */
    public function addPages()
    {
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page(
                [
                    'page_title'  => __('Algolia', 'jabbado'),
                    'menu_slug'   => 'algolia',
                    'parent_slug' => 'options-general.php',
                    'capability'  => 'create_users',
                ]
            );
        }
    }
}
