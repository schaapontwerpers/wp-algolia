<?php

namespace WordPressAlgolia\CLI;

use WordPressAlgolia\Index\Posts;
use WordPressAlgolia\Index\Sorting;
use WordPressAlgolia\Index\Taxonomies;

class Algolia
{
    /**
     * Add custom rewrite rules
     *
     * @return void
     */
    public function init()
    {
        $this->reindex();
        $this->setfacets();
        $this->setsorting();
    }

    /**
     * Add custom rewrite rules
     *
     * @return void
     */
    public function reindex()
    {
        $posts = new Posts();
        $posts->reindex();
    }

    /**
     * Add custom rewrite rules
     *
     * @return void
     */
    public function setfacets()
    {
        $posts = new Taxonomies();
        $posts->setFacets();
    }

    /**
     * Add custom rewrite rules
     *
     * @return void
     */
    public function setsorting()
    {
        $posts = new Sorting();
        $posts->setReplicas();
    }
}
